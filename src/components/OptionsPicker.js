import React, { Component } from 'react';
import { View, Text, Picker } from 'react-native';

export default class OptionsPicker extends Component {
    render() {
        let {parentContainerStyle,
            labelName,selectedValue,
            inputFieldStyle,onChangeText,optionValues,name} = this.props
        return (
            <View style={parentContainerStyle}>
          <Text>{labelName}</Text>
                <Picker
                    selectedValue={selectedValue}
                    style={inputFieldStyle}
                    onValueChange={(text)=>onChangeText(name,text)}>
                    {
                        optionValues.map((item)=>{
                            return(
                                <Picker.Item label={item.name} value={item.name}
                                 key={item.id}/>
                            )
                            
                        })
                    }
                </Picker>
            </View>
        );
    }
}
