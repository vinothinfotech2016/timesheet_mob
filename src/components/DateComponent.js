import React, { Component } from 'react';
import { View, Text } from 'react-native';
import DatePicker from 'react-native-datepicker';
export default class DateInput extends Component {
    render() {
        let firstDate=new Date(new Date().getFullYear(), 0, 1);
        let lastDate=new Date(new Date().getFullYear(), 11, 31);
        return (
            <View style={this.props.containerStyle}>
                <Text>{this.props.labelName}</Text>
                <DatePicker
                    style={this.props.style}
                    date={this.props.date}
                    mode="date"
                    placeholder="mm/mm/mm"
                    format="YYYY-MM-DD"
                    minDate={firstDate}
                    maxDate={lastDate}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    showIcon={false}
                    customStyles={{
                        dateIcon: {
                            display: "none",
                            position: 'absolute',
                            left: 0,
                            top: 4,
                            marginLeft:0
                        },
                        dateInput: {
                            borderLeftWidth: 0,
                            borderRightWidth: 0,
                            borderTopWidth: 0,
                            borderBottomWidth: 0,
                        }
                    }}
                    onDateChange={(date)=> this.props.onDateChange(this.props.name, date)}
                />
            </View>
        );
    }
}