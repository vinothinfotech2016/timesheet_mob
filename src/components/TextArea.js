import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Textarea from 'react-native-textarea';

export default class TextArea extends Component {
    render() {
        return (
            <View style={this.props.TextAreaContainerStyle}>
               <Text>{this.props.labelName}</Text>
                <Textarea
                    containerStyle={this.props.containerStyle}
                    style={this.props.style}
                    onChangeText={(text)=>this.props.onChangeText(this.props.name,text)}
                    defaultValue={this.props.defaultValue}
                    maxLength={120}
                    placeholder={'type something'}
                    placeholderTextColor={'#c7c7c7'}
                    underlineColorAndroid={'transparent'}
                    
                />
            </View>
        );
    }
}
