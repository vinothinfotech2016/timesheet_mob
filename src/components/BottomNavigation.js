import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
export default class BottomNavigation extends Component {
    state = {
        activeColor:{
            notify:"red",
            timeSheet:"black",
            Leave:"black",
            settings:"black"
        }
    }
    onPressNavigate = (value) => {
       let {activeColor} = this.state
       Object.keys(activeColor).map((item)=>{
           if(item==value){
            activeColor[item]="red"
            this.setState({
                activeColor:activeColor
            })
           }
           else{
            activeColor[item]="black"
            this.setState({
                activeColor:activeColor
            })
           }
       });
       if(value=="timeSheet"){
        this.props.navigateTo("TimesheetScreen");
       }
       else if(value=="Leave"){
        this.props.navigateTo("LeaveScreen");
       }
       else if(value=="settings"){
           this.props.navigateTo("SettingsScreen");
       }
       else if(value=="notify"){
        this.props.navigateTo("NotificationsScreen");
       }
    }
    render() {
        let {notify,timeSheet,Leave,settings} = this.state.activeColor
        return (
            <View style={styles.bottomNavigationContainer}>
                <TouchableOpacity style={styles.NavigationIconContainer}
                    onPress={()=>this.onPressNavigate("notify")}>
                    <Ionicons name="ios-notifications-outline" size={40} color={notify} />
                    <Text style={{color:notify}}>Notification</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.NavigationIconContainer}
                    onPress={()=>this.onPressNavigate("timeSheet")} >
                    <MaterialIcons name="timer" size={40} color={timeSheet} />
                    <Text style={{color:timeSheet}}>Timesheet</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.NavigationIconContainer}
                    onPress={()=>this.onPressNavigate("Leave")}>
                    <Ionicons name="ios-calendar" size={40} color={Leave} />
                    <Text style={{color:Leave}}>Leave</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.NavigationIconContainer}
                    onPress={()=>this.onPressNavigate("settings")}>
                    <MaterialCommunityIcons name="settings" size={40} color={settings} />
                    <Text style={{color:settings}}>Settings</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    bottomNavigationContainer: {
        height: "100%",
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    NavigationIconContainer: {
        height: "100%",
        width: "22%",
        alignItems: "center",
        justifyContent: "center"
    },
})