import React, { Component } from 'react';
import {
    View, TextInput, Text,key,
} from 'react-native';
export default class TextInputField extends Component {
    render() {
        let {keyboardType,placeholder,onChangeText,value,labelStyle,
            labelName,parentStyle,secureText,name,editable,maxLength,
        childStyle} = this.props
        return (  
            <View style={parentStyle}>
                <Text style={labelStyle}>{labelName}</Text>
                <TextInput
                    keyboardType={keyboardType}
                    maxLength={maxLength}
                    editable={editable}
                    placeholder={placeholder}
                    onChangeText={(text)=>onChangeText(name,text)}
                    value={value}
                    style={childStyle}
                    secureTextEntry={secureText}
                />
            </View>
        );
    }
}