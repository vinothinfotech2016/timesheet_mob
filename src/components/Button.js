import React, { Component } from 'react';
import { View, Text,TouchableOpacity } from 'react-native';

export default class SubmitButton extends Component {
  render() {
    return (
        <TouchableOpacity style={this.props.style}
        onPress={this.props.onPress}>
                  <Text style={{ fontSize: 20 }}>{this.props.title}</Text>
         </TouchableOpacity>
    );
  }
}
