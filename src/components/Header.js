import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
export default class Header extends Component {
    render() {
        return (
            <View style={styles.Header}>
                <View style={styles.userDetailContainer}>
                    <Text style={{ fontSize: 15 }}>{this.props.userfirstName}</Text>
                    <Text style={{ fontSize: 15 }}>{this.props.userEmail}</Text>
                </View>
                <View style={styles.userDetailContainer}>
                    <Text style={{ fontSize: 20, fontWeight: "bold" }}>{this.props.HeaderTitle}</Text>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    Header: {
        height: "100%", width: "100%", justifyContent: "space-evenly"
    },
    userDetailContainer: {
        height: "50%", width: "100%", justifyContent: "flex-end"
    }
})