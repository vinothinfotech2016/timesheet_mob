import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import LoginScreen from '../modules/LoginScreen';
import ForgotPwdScreen from '../modules/ForgotPwdScreen';
import NotificationsScreen from '../modules/NotificationsScreen';
import LeaveScreen from '../modules/LeaveScreen';
import TimesheetScreen from '../modules/TimesheetScreen';
import SettingsScreen from '../modules/SettingsScreen';
import { Ionicons, MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import TimeSheetFormScreen from '../modules/TimeSheetFormScreen';
import EditProfile from '../modules/EditProfile';
const notifyTab = createStackNavigator({
    NotificationsScreen:NotificationsScreen,
},{
    defaultNavigationOptions:{
        headerShown:false
    }
})
const timesheetTab = createStackNavigator({
    TimesheetScreen:TimesheetScreen
},{
defaultNavigationOptions:{
    title:"My Calendar" }
})
const LeaveTab = createStackNavigator({
    Leave:LeaveScreen,
})
const settingTab = createStackNavigator({
    Settings:SettingsScreen,
})
const BottomNavigator = createBottomTabNavigator({
    Notification:notifyTab,
    Timesheet:timesheetTab,
    Leave:LeaveTab,
    Setting:settingTab
}, {
    defaultNavigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
            const { routeName } = navigation.state;
            if (routeName === 'Notification') {
                return (
                    <Ionicons name="ios-notifications-outline" size={32} color={tintColor} />
                );
            } else if (routeName === 'Timesheet') {
                return (
                    <MaterialIcons name="timer" size={32} color={tintColor} />
                );
            }
            else if (routeName === 'Leave') {
                return (
                    <Ionicons name="ios-calendar" size={32} color={tintColor} />
                );
            }
            else {
                return (
                    <MaterialCommunityIcons name="settings" size={32} color={tintColor} />
                );
            }
        },
    }),
    tabBarOptions: {
        activeTintColor: '#FF6F00',
        inactiveTintColor: '#263238',
        style:{
            height:56,
        }
    },
}
)
const MainNavigator = createStackNavigator({
    LoginScreen: {
        screen: LoginScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    ForgotPwdScreen: {
        screen: ForgotPwdScreen,
        navigationOptions: {
            title:"Forgot Password"
        }
    },
    TimeSheetFormScreen:{
        screen: TimeSheetFormScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    HomeScreen: {
        screen: BottomNavigator,
        navigationOptions: {
            headerShown: false
        }
    },
    EditProfile:{
        screen:EditProfile,
        navigationOptions:{
            title:"Edit Profile"
        }
    }
});
const AppContainer = createAppContainer(MainNavigator);
export default AppContainer;
