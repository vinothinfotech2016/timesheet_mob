import axios from 'axios';
import { BASEURL } from './config';

export const appApi = axios.create({
    baseURL: BASEURL,
    responseType: 'json',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer 4c0f488d-3a90-4e83-886a-f95e916b7f66`
    }
});
export const login = (data) => appApi.post('login', data);
export const forgetPassword = (value) => appApi.get('forgetPassword?email=' + value)
export const LeaveTypes = () => appApi.get('LeaveTypes');
export const createLeave = (leaveData) => appApi.post('leave', leaveData);
export const leaveReport = (leaveReport) => appApi.get('leaveReport', leaveReport);
export const getProjectByEmployee = (id) => appApi.get("projectByEmp/" + id);
export const getTaskByProject = (id) => appApi.get('project/' + id);
export const sendFilledTimeSheetData = (data) => appApi.post('timesheet',data);
export const getTimesheetByemp = (data)=>appApi.post('timesheetByemp',data);
export const getAllemployeeReport = (data)=>appApi.post('timesheet/filter',data);
export const getAllProjectsStatus = ()=>appApi.get('projects');