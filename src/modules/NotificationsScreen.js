import React, { Component } from 'react';
import { View, Text, StyleSheet, Alert, TouchableOpacity, ScrollView } from 'react-native';
import Header from '../components/Header';
import GestureRecognizer, { swipeDirections } from 'react-native-swipe-gestures';
import { getAllProjectsStatus } from '../config/apiconfig';
export default class NotificationsScreen extends Component {
    state = {
        userEmail: "",
        isClickedPending: true,
        listOfProjects: {}
    }
    componentDidMount = () => {
        getAllProjectsStatus().then(response => {
            let { data } = response
            let fetchedData = { ...data }
            this.setState({
                listOfProjects: fetchedData
            })
        }).catch(error => {
            console.log(error.response);
        })
    }
    onSwipeLeft = () => {
        let { isClickedPending } = this.state
        isClickedPending = isClickedPending == true ? false : false
        this.setState({
            isClickedPending: isClickedPending
        })
    }
    onSwipeRight = () => {
        let { isClickedPending } = this.state
        isClickedPending = isClickedPending == false ? true : true
        this.setState({
            isClickedPending: isClickedPending
        })
    }
    selectProjectType = (e) => {
        let { isClickedPending } = this.state
        if (e == "pending") {
            isClickedPending = true
            this.setState({
                isClickedPending: isClickedPending
            },
            )
        } else {
            isClickedPending = false
            this.setState({
                isClickedPending: isClickedPending
            },
            )
        }
    }
    render() {
        const config = {
            velocityThreshold: 0.3,
            directionalOffsetThreshold: 80
        };
        let { email, firstName } = this.props.navigation.state.params
        let { isClickedPending, listOfProjects } = this.state
        return (
            <View style={{ height: "100%", width: "100%", padding: 20 }}>
                <View style={{ height: "15%", width: "100%", marginBottom: 10 }}>
                    <Header
                        userfirstName={firstName}
                        userEmail={email}
                        HeaderTitle="My Notifications" />
                </View>
                <View style={{ justifyContent: "space-between", flexDirection: "row", alignItems: "center", height: "8%", margin: 0 }}>
                    <TouchableOpacity
                        onPress={() => this.selectProjectType("pending")}
                        style={isClickedPending ? styles.activeBorder : styles.inActiveBorder}>
                        <Text style={{ color: isClickedPending ? "green" : "black" }}>Pending</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.selectProjectType("completed")}
                        style={isClickedPending ? styles.inActiveBorder : styles.activeBorder}>
                        <Text style={{ color: isClickedPending ? "black" : "green" }}>Completed</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.lineStyle} />
                <GestureRecognizer
                    onSwipeLeft={this.onSwipeLeft}
                    onSwipeRight={this.onSwipeRight}
                    config={config}
                    style={{ height: "80%", width: "100%", }}
                >
                    <ScrollView style={{ height: "100%", width: "100%" }}>
                        {
                            Object.keys(listOfProjects).map((item, index) => {
                                if (isClickedPending) {
                                    if (listOfProjects[item].status != "Completed") {
                                        return (
                                            <View key={listOfProjects[item].name} style={
                                                { height: "10%", width: "100%", 
                                                 justifyContent: "center", alignItems: "center",
                                                  backgroundColor: "white",marginBottom:5,marginTop:5,borderRadius:20
                                                   }}>
                                                <Text style={{ textAlign: "center", color: "red", fontSize: 20 }}>{listOfProjects[item].name}</Text>
                                                <Text>Starts From : {listOfProjects[item].startedDate.slice(0,10)}</Text>
                                                <Text>Status : {listOfProjects[item].status}</Text>
                                                <Text>Estimated Hours : {listOfProjects[item].totalhours}</Text>
                                                <Text>Employees Count : {listOfProjects[item].assignTo.length}</Text>
                                            </View>
                                        )
                                    }
                                }
                                else {
                                    if (listOfProjects[item].status == "Completed") {
                                        return (
                                            <View key={listOfProjects[item].name} style={
                                                { height: 200, width: "100%", justifyContent: "center",
                                                 alignItems: "center", backgroundColor: "white",
                                                 borderRadius:20 }} >
                                                <Text style={{ textAlign: "center", color: "red", fontSize: 20 }}>{listOfProjects[item].name}</Text>
                                                <Text>Starts From : {listOfProjects[item].startedDate.slice(0,10)}</Text>
                                                <Text>Status : {listOfProjects[item].status}</Text>
                                                <Text>Estimated Hours : {listOfProjects[item].totalhours}</Text>
                                                <Text>Employees Count : {listOfProjects[item].assignTo.length}</Text>
                                            </View>
                                        )
                                    }
                                }
                            })
                        }
                    </ScrollView>
                </GestureRecognizer>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    lineStyle: {
        borderWidth: 0.5,
        borderColor: 'black',
        margin: 0,
    },
    card: {
        height: 532,
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "grey"
    },
    activeBorder: {
        height: "100%",
        width: "25%",
        justifyContent: "center",
        alignItems: "center",
        margin: 0, borderBottomWidth: 3,
        borderBottomColor: "green", color: "green",
    },
    inActiveBorder: {
        height: "100%",
        width: "25%",
        justifyContent: "center",
        alignItems: "center",
        margin: 0,
    }
});