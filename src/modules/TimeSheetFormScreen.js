import React, { Component } from 'react';
import { View, Text, StyleSheet, AsyncStorage } from 'react-native';
import OptionsPicker from '../components/OptionsPicker';
import TextInputField from '../components/TextInput';
import TextArea from '../components/TextArea';
import { Ionicons, Entypo } from '@expo/vector-icons';
import {
    getProjectByEmployee, getTaskByProject, sendFilledTimeSheetData,
} from '../config/apiconfig';
import SubmitButton from '../components/Button';
export default class TimeSheetFormScreen extends Component {
    state = {
        projectTasks: [],
        employeeProjects: [],
        data: {
            projectName: "",
            taskName: "",
            date: "",
            taskId: null,
            spendedTime:"",
            description:"",
            projectId: null
        },
        dataArray: [],
        timesheetFilter: {
            fromDate: "2019-01-09",
            toDate: ""
        },
        isFilledAlready: true,
        alreadyFilledData: this.props.navigation.state.params.filledData
    };
    componentDidMount = async () => {
        let empId = await AsyncStorage.getItem('empId');
        getProjectByEmployee(empId).then(response => {
            let projectSeeds = response.data;
            this.setState({
                employeeProjects: projectSeeds
            });
        }).catch(error => {
            console.log(error);
        });
    };
    selectTaskByProject = async () => {
        let { projectId } = this.state.data;
        let { data } = await getTaskByProject(projectId);
        let fetchedTasksByProject = data.tasks
        this.setState({ projectTasks: fetchedTasksByProject });
    }
    fillTimeSheet = async ()=>{
        let {isFilledAlready} = this.state
        this.setState({
            isFilledAlready:false
        })
    }
    onChangeText = async (name, value) => {
        let { data, employeeProjects, projectTasks } = this.state;
        data[name] = value;
        this.setState({
            data:data
        })
        if (name == "projectName") {
            employeeProjects.map((item) => {
                if (value == item.name) {
                    data.projectId = item.id;
                    this.setState({ data: data }, () => { this.selectTaskByProject(); });
                }
            })
        }
        else if (name == "taskName") {
            projectTasks.map((item) => {
                if (value == item.name) {
                    data.taskId = item.id;
                    this.setState({ data: data });
                }
            })
        }
    }
    sendData = async () => {
        let { dataArray } = this.state
        let newArray = dataArray
        sendFilledTimeSheetData(newArray).then(response => {
            alert("timesheet created successfully");
        }).catch(error => { console.log(error.response) })
    }
    validateData = ()=>{
        let {data} = this.state
        let isValid = true;
        Object.keys(data).map((item,index)=>{
           if(item=="spendedTime"&& data[item]==""){
               alert("Please fill All the fields");
               isValid = false;
           }
           else if(item=="description" && data[item]==""){
               alert("Please fill All the Fields");
               isValid = false;
           }
        })
        return isValid;
    }
    onSubmitData = () => {
        let { data, dataArray } = this.state
        let { spendedTime } = this.state.data
        let newData = { ...data }
        let isValid = this.validateData();
        if(isValid){
            let { fillThisDate } = this.props.navigation.state.params;
            newData.date = fillThisDate
            newData.spendedTime = Number(spendedTime);
            let newArray = [...dataArray, newData];
            this.setState({
                dataArray: newArray
            }, () => { this.sendData() });
             data.spendedTime ="";
             data.description ="";
               this.setState({
                  data:data 
               })
        }
    }
    render() {
        let { projectTasks, employeeProjects, data, isFilledAlready } = this.state;
        let { fillThisDate, filledData } = this.props.navigation.state.params;
        let formatedDate = new Date(fillThisDate);
        let displayHeaderdate = formatedDate.toString().split(" ");
        return (
            <View style={styles.masterContainer}>
                <View style={{ height: "10%", width: "100%", marginTop: 20 }}>
                    <View style={{
                        flexDirection: "row", padding: 20,
                        justifyContent: "space-between", width: "100%", height: "100%", alignItems: "center"
                    }}><Ionicons name="ios-arrow-back" size={32} 
                    onPress={()=>this.props.navigation.navigate("TimesheetScreen")}/>
                        <Text style={{ fontSize: 18 }}>{displayHeaderdate[2] + " " + displayHeaderdate[1]}</Text>
                        {isFilledAlready ? <Entypo name="plus" size={32} color={"red"} 
                        onPress={this.fillTimeSheet}/> : <Text></Text>}
                    </View>
                </View>
                {
                    isFilledAlready ?
                        <View style={{
                            height: "70%", width: "100%",flexDirection:"column",justifyContent: "center",
                            alignItems: "center"
                        }}>{
                            filledData.length == 0 ? <Text>No Data Found</Text> : 
                            <View style={{height:"100%",width:"100%",flexDirection:"column",
                             padding: 20 }}>{
                                filledData.map((item,index)=>{
                                    return(
                                    <View key={index} style={{height:"10%",width:"100%",
                                    justifyContent:"space-between",flexDirection:"row",
                                    alignItems:"center",
                                    paddingLeft: 20,
                                    paddingRight: 20,
                                    marginTop:5,
                                    backgroundColor: "#B7C9E9"}}>
                                    <Text>{item.description}</Text>
                                    <Text>{item.spendedTime}H</Text>
                                    </View>
                                    )
                                })  
                                }         
                            </View>
                        }
                        </View> :
                        <View style={{ height: "70%", width: "100%", padding: 20 }}>
                            <OptionsPicker
                                inputFieldStyle={{ backgroundColor: "#B7C9E9", }}
                                labelName={"Project Name"}
                                optionValues={employeeProjects}
                                name={"projectName"}
                                selectedValue={data.projectName}
                                onChangeText={this.onChangeText}
                            />
                            <OptionsPicker
                                parentContainerStyle={{ marginTop: 10 }}
                                inputFieldStyle={{ backgroundColor: "#B7C9E9" }}
                                labelName={"Task Name"}
                                optionValues={projectTasks}
                                name={"taskName"}
                                selectedValue={data.taskName}
                                onChangeText={this.onChangeText}
                            />
                            <TextInputField
                                keyboardType={'numeric'}
                                onChangeText={this.onChangeText}
                                maxLength={1}
                                value={data.spendedTime}
                                name={"spendedTime"}
                                labelName={"Time Spent(in hours)"}
                                parentStyle={{ height: "20%", marginTop: 10, }}
                                childStyle={{ height: "55%", backgroundColor: "#B7C9E9", paddingLeft: 170, }}
                            />
                            <TextArea
                                onChangeText={this.onChangeText}
                                containerStyle={{ height: "100%" }}
                                labelName={"Description"}
                                TextAreaContainerStyle={{ height: "20%", width: "100%", }}
                                style={styles.textAreaField}
                                name={"description"}
                                defaultValue={data.description}
                            />
                            <View style={styles.buttonContainer}>
                                <SubmitButton
                                    style={styles.SubmitButtonStyle}
                                    title="Submit"
                                    onPress={this.onSubmitData}
                                />
                            </View>
                        </View>}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    masterContainer: {
        height: "100%",
        width: "100%",
    },
    buttonContainer: {
        height: "20%", width: "100%", marginTop: 20,
        flexDirection: "row", justifyContent: "flex-end"
    },
    SubmitButtonStyle: {
        height: "50%", width: "30%", alignItems: "center",
        borderRadius: 15, justifyContent: "center",
        backgroundColor: "#AEEEB1"
    },
    textAreaField: {
        height: "70%", width: "100%", backgroundColor: "#B7C9E9"
    }
})