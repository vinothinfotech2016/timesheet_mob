import React, { Component } from 'react';
import { View, Text, TouchableOpacity,StyleSheet } from 'react-native';
import { FontAwesome5, Entypo } from '@expo/vector-icons';
export default class SettingsScreen extends Component {
  logoutSession = () => {
    this.props.navigation.navigate("LoginScreen");
  }
  gotoEditScreen = ()=>{
    this.props.navigation.navigate("EditProfile");
  }
  render() {
    return (
      <View style={{ height: "100%", width: "100%", padding: 20, }}>
        <TouchableOpacity style={styles.linkStyle} onPress={this.gotoEditScreen}>
          <FontAwesome5 name="user-edit" size={32}/>
          <Text style={{ marginLeft: 10 }}>Edit Profile</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.linkStyle}>
          <Entypo name="fingerprint" size={32} />
          <Text style={{ marginLeft: 10 }}>Fingerprint,password & Pattern</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.linkStyle}
          onPress={this.logoutSession}>
          <Entypo name="log-out" size={32} />
          <Text style={{ marginLeft: 10 }}>Logout</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
     linkStyle:{
      height: "10%",
      flexDirection: "row",
      alignItems: "center"
     }
})