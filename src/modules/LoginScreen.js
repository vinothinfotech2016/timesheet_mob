import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity,AsyncStorage,KeyboardAvoidingView } from 'react-native';
import TextInputField from '../components/TextInput';
import SubmitButton from '../components/Button';
import { login } from '../config/apiconfig';
import { styles } from '../styles/styles';
export default class LoginScreen extends Component {
  state = {
    inputTypes: [
      { name: "email", labelName: "Username", value: "", key: "username" },
      { name: "password", labelName: "Password", value: "", key: "password", isSecure: true }
    ],
    userCredential: {
      email: "",
      password: ""
    },
    errorMsg: [],
    validationAlert: [],
  }
  onChangeUserCredential = (fieldName, value) => {
    let { userCredential } = this.state
    userCredential[fieldName] = value
    this.setState({
      userCredential: userCredential
    })
  }
  validateData = () => {
    let { userCredential, errorMsg, validationAlert } = this.state
    let isValid = true
    Object.keys(userCredential).map((item, index) => {
      switch (item) {
        case "email":
          if (userCredential[item] == "") {
            errorMsg.push(item);
            this.setState({ errorMsg: errorMsg });
            isValid = false;
          }
          else if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/).test(userCredential[item])) {
            validationAlert.push(item);
            this.setState({ validationAlert: validationAlert });
            isValid = false;
          }
          break;
        case "password":
          if (userCredential[item] == "") {
            errorMsg.push(item);
            this.setState({ errorMsg: errorMsg });
            isValid = false;
          }
          break;
      }
    })
    return isValid;
  }
  loginUser = async () => {
    let { userCredential, errorMsg, validationAlert } = this.state
    let isValid = this.validateData();
    if (isValid) {
      try {
        let { data } = await login(userCredential);
        this.props.navigation.navigate("NotificationsScreen", {
          email: data.email,
          firstName: data.firstName
        });
        AsyncStorage.setItem('empId',JSON.stringify(data.id));
        AsyncStorage.setItem('empName',JSON.stringify(data.firstName));
        AsyncStorage.setItem('empEmail',JSON.stringify(data.email));
        AsyncStorage.setItem('empToken',JSON.stringify(data.token));
        userCredential.email="";
        userCredential.password="";
        this.setState({
          userCredential:userCredential
        })
      } catch (err) {
        console.log(err.response.data.error.message);
      }
    }
    else {
      if (errorMsg != "") {
        alert("Please fill the following fields" + "\n" + errorMsg.map((value) => "\n" + value))
      }
      else if (errorMsg == "" && validationAlert != "") {
        alert("Check below fields are valid" + "\n" + validationAlert.map((value) => "\n" + value))
      }
      errorMsg = [];
      validationAlert = [];
      this.setState({
        errorMsg: errorMsg,
        validationAlert: validationAlert
      })
    }
  }
  render() {
    let { inputTypes, userCredential } = this.state
    return (
      <View style={styles.LoginScreen}>
        <View style={styles.LoginheaderContainer}>
          <Image source={require('../assets/Virdhi_Logo.png')}
            style={styles.logo} />
        </View>
        <View style={styles.loginBodyContainer}>
          <KeyboardAvoidingView style={{ height: "100%", width: "100%" }}>
            {
              inputTypes.map((value) => {
                return (
                  <TextInputField
                    keyboardType={value.type === 'number' ? 'numeric' : 'default'}
                    onChangeText={this.onChangeUserCredential}
                    value={userCredential[value.name]}
                    labelName={value.labelName}
                    name={value.name}
                    parentStyle={styles.loginInputFieldsContainer}
                    childStyle={styles.loginInputField}
                    labelStyle={{ fontSize: 20 }}
                    secureText={value.key === 'password' ? true : false}
                    key={value.key}
                  />
                )
              })
            }
            <View style={{
              flexDirection: "row", justifyContent: "flex-end",
              height: "30%", width: "100%", marginTop: 20,
            }}><Text style={{ fontSize: 20, }}
              onPress={() => this.props.navigation.navigate("ForgotPwdScreen")}>Forgot Password?</Text></View>
          </KeyboardAvoidingView>
        </View>
        <View style={styles.loginFooterContainer}>
          <SubmitButton
            style={{
              height: "35%", width: "30%", alignItems: "center",
              borderRadius: 15, justifyContent: "center", backgroundColor: "#AEEEB1"
            }}
            onPress={this.loginUser}
            title="Login" />
        </View>
      </View>
    );
  }
}