import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, AsyncStorage } from 'react-native';
import TextInputField from '../components/TextInput';
import { FontAwesome } from '@expo/vector-icons';
import SubmitButton from '../components/Button';

export default class EditProfile extends Component {
    state = {
        uploadImage: "../assets/Virdhi_Logo.png",
        empName:"",
        empEmail:""
    }
    componentDidMount = async()=>{
        let mail = await AsyncStorage.getItem('empEmail');
        let empName = await AsyncStorage.getItem('empName');
        console.log(mail);
        this.setState({
            empName:empName,
            empEmail:mail
        })
    }
    render() {
        let {empName,empEmail} = this.state
        return (
            <View style={{ height: "100%", width: "100%", padding: 20, }}>
                <View style={styles.uploadImageContainer}>
                    <View style={styles.imageContainer}>
                        <FontAwesome name="upload" size={32} />
                    </View>
                    <Text style={{marginTop:10}}>Upload Profile Picture</Text>
                </View>
                <View style={{ height: "20%" }}>
                    <TextInputField
                        keyboardType={'default'}
                        onChangeText={(text) => this._onChangeText(text)}
                        value={empName}
                        editable={false}
                        labelName={"Name"}
                        labelStyle={{fontSize:18}}
                        parentStyle={styles.inputFieldContainer}
                        childStyle={styles.inputField}
                    // key={value.key}
                    />
                    <TextInputField
                        keyboardType={'default'}
                        onChangeText={(text) => this._onChangeText(text)}
                        value={empEmail}
                        labelStyle={{fontSize:"100"}}
                        labelName={"Email"}
                        editable={false}
                        parentStyle={styles.inputFieldContainer}
                        childStyle={styles.inputField}
                        labelStyle={{fontSize:18}}
                    // key={value.key}
                    />
                </View>
                <View style={styles.ButtonContainer}>
                    <SubmitButton
                        style={styles.SubmitButton}
                        title="Submit"
                    />
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    inputFieldContainer: {
        height: "50%",
        width: "100%"
    },
    uploadImageContainer: {
        justifyContent: "center",
        alignItems: "center",
        height: "20%",
        width: "100%"
    },
    imageContainer: {
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 60,
        borderWidth: 1,
        height: "60%",
        width: "22%"
    },
    inputField: {
        borderBottomWidth: 1
    },
    ButtonContainer:{
        height: "20%", width: "100%",
        flexDirection: "row", justifyContent: "flex-end",
        alignItems:"center"
    },
    SubmitButton: {
        height: "34%", width: "30%", alignItems: "center",
        borderRadius: 15, justifyContent: "center", backgroundColor: "#AEEEB1"
    }
})
