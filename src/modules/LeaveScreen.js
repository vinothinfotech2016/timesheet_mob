import React, { Component } from 'react';
import { View, Text, Alert } from 'react-native';
import DateInput from '../components/DateComponent';
import OptionsPicker from '../components/OptionsPicker';
import { LeaveTypes, createLeave } from '../config/apiconfig';
import SubmitButton from '../components/Button';
import TextArea from '../components/TextArea';
export default class LeaveScreen extends Component {
    state = {
        data: {
            fromDate: "",
            toDate: "",
            leaveTypeId: null,
            reason: "",
        },
        leaveOptions: [],
        errorMsg: [],
        validationAlert: [],
        totalAnnualLeave: 15
    }
    componentDidMount = async () => {
        let { data } = await LeaveTypes();
        this.setState({
            leaveOptions: data
        })
    }
    validateData = () => {
        let { data, errorMsg, validationAlert } = this.state
        let { fromDate, toDate } = data
        let isValid = true
        let isValidDates = fromDate <= toDate
        Object.keys(data).map((item, index) => {
            switch (item) {
                case "fromDate":
                    if (data[item] == "") {
                        errorMsg.push(item);
                        this.setState({ errorMsg: errorMsg });
                        isValid = false;
                    }
                    break;
                case "toDate":
                    if (data[item] == "") {
                        errorMsg.push(item);
                        this.setState({ errorMsg: errorMsg });
                        isValid = false;
                    }
                    else if (!isValidDates) {
                        validationAlert.push(item);
                        this.setState({ validationAlert: validationAlert });
                        isValid = false;
                    }
                    break;
            }
        })
        return isValid;
    }
    isConfirmLeave = async () => {
        let { data } = this.state;
        let newData = { ...data }
        createLeave(newData).then(response => {
            console.log(response);
            data.fromDate="";
            data.toDate="";
            data.reason = "";
            this.setState({data:data});
        }).catch(error => {
            alert(error.response.data.error.message);
            data.fromDate="";
            data.toDate="";
            data.reason = "";
            this.setState({data:data});
        }
        )
    }
    submitLeave = () => {
        let { errorMsg, validationAlert } = this.state;
        let isValid = this.validateData();
        if (isValid) {
            Alert.alert(
                '',
                'Are you sure want to submit?',
                [
                    {
                        text: 'CANCEL',
                        onPress: () => { return false },
                        style: 'cancel',
                    },
                    { text: 'OK', onPress: () => this.isConfirmLeave() },
                ],
                { cancelable: false },
            );
        }
        else {
            if (errorMsg != "") {
                alert("Please Fill The Following Fields" + "\n" + errorMsg.map((value) => "\n" + value));
            }
            else if (errorMsg == "" && validationAlert != "") {
                alert("From-date should not be greater than To-date");
            }
            errorMsg = [];
            validationAlert = [];
            this.setState({
                errorMsg: errorMsg,
                validationAlert: validationAlert
            })
        }
    }
    onChangeText = (name, value) => {
        let { data, leaveOptions } = this.state
        data[name] = value
        if (name == "leaveType") {
            leaveOptions.map((item) => {
                if (item.name == value) {
                    data.leaveTypeId = item.id
                    this.setState({
                        data: data
                    })
                }
            })
        }
        this.setState({
            data: data
        })
    }
    render() {
        let { data, leaveOptions, totalAnnualLeave } = this.state
        return (
            <View style={{ height: "100%", width: "100%", padding: 20 }}>
                <View style={{ height: "10%", justifyContent: "flex-end", flexDirection: "row" }}>
                    <View style={{ height: "100%", }}>
                        <Text>Annual Leave : {totalAnnualLeave}</Text>
                        <Text>Leaves Taken :</Text>
                    </View>
                </View>
                <View style={{ height: "60%", width: "100%", flexDirection: "row", flexWrap: "wrap", justifyContent: "space-between" }}>
                    <DateInput
                        containerStyle={{ height: "20%", width: "45%" }}
                        style={{ height: "70%", width: "100%", backgroundColor: "#B7C9E9", paddingTop: 5 }}
                        date={data.fromDate}
                        labelName={"From"}
                        name={"fromDate"}
                        onDateChange={this.onChangeText}
                    />
                    <DateInput
                        containerStyle={{ height: "20%", width: "45%" }}
                        style={{ height: "70%", width: "100%", backgroundColor: "#B7C9E9", paddingTop: 5 }}
                        date={data.toDate}
                        labelName={"To"}
                        name={"toDate"}
                        onDateChange={this.onChangeText}
                    />
                    <Text style={{ height: "10%", width: "100%" }}>No of Leaves</Text>
                    <OptionsPicker
                        parentContainerStyle={{ height: "20%", width: "100%" }}
                        inputFieldStyle={{ backgroundColor: "#B7C9E9", }}
                        selectedValue={data.leaveType}
                        onChangeText={this.onChangeText}
                        labelName={"Leave Type"}
                        name={"leaveType"}
                        optionValues={leaveOptions}
                    />
                    <TextArea
                        onChangeText={this.onChange}
                        labelName={"Reason"}
                        name={"reason"}
                        defaultValue={data.reason}
                        onChangeText={this.onChangeText}
                        containerStyle={{ height: "100%", width: "100%" }}
                        style={{ height: "100%", backgroundColor: "#B7C9E9", paddingLeft: 20, color: "black" }}
                        TextAreaContainerStyle={{ height: "25%", width: "100%", marginTop: 10, }}
                    />
                </View>
                <View style={{ height: "20%", width: "100%", flexDirection: "row", justifyContent: "flex-end" }}>
                    <SubmitButton
                        style={{
                            height: "50%", width: "30%", alignItems: "center",
                            borderRadius: 15, justifyContent: "center", backgroundColor: "#AEEEB1"
                        }}
                        title="Submit"
                        onPress={this.submitLeave}
                    />
                </View>
            </View>
        );
    }
}
