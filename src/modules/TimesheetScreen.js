import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,AsyncStorage
} from 'react-native';
import CalendarPicker from 'react-native-calendar-picker';
import { getTimesheetByemp } from '../config/apiconfig';
export default class TimeSheet extends Component {
    state = {
      selectedDate:'',
      data:{
        fromDate: "",
        toDate: "",
        employeeId:null
      },
    }
    componentDidMount = async()=>{
      let {...data} = this.state
      let employeeId = await AsyncStorage.getItem('empId');
      data.employeeId = employeeId
      this.setState({
        data:data,
      })
  }
    sendPickedDateToTimesheet=async()=>{
      let {selectedDate,data}=this.state
      let newData = {...data}
      getTimesheetByemp(newData).then(response=>{
        let filledData = response.data
        // if(filledData.length == 0){
          this.props.navigation.navigate("TimeSheetFormScreen",{
            fillThisDate:selectedDate,
            filledData:filledData
          });
        // }
        // else{
        //   this.props.navigation.navigate("TimeSheetFormScreen",{
        //     fillThisDate:selectedDate,
        //     filledData:filledData,
        //     isFilledAlready:true
        //   });
        // }
      }).catch(error=>{
        console.log(error);
      })
    }
    onDateChange=(date)=>  {
      let pickedDate = new Date(date).toLocaleDateString();
      let {data}=this.state;
      let newData = {...data}
      newData.fromDate=pickedDate,
      newData.toDate=pickedDate
      this.setState({
        selectedDate: pickedDate,
        data:newData
      },()=>{this.sendPickedDateToTimesheet()});
    }
  render() {
    const { selectedStartDate } = this.state;
    return (
      <View style={styles.container}>
      <CalendarPicker
        onDateChange={this.onDateChange}
        previousTitle="<<"
        nextTitle=">>"
        weekdays={['S', 'M', 'T', 'W', 'T', 'F', 'S']}
      />
    </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    marginTop: 100,
  },
});