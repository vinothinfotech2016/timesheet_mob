import React, { Component } from 'react';
import { View, Text,Alert } from 'react-native';
import TextInputField from '../components/TextInput';
import SubmitButton from '../components/Button';
import { forgetPassword } from '../config/apiconfig';

export default class ForgotPwdScreen extends Component {
    state = {
        email: ""
    }
    // resetPassword = () => {
    //     this.props.navigation.navigate("LoginScreen")
    // }
    onChangeText = (name,value)=>{
        let {...email} = this.state
             email = value
             this.setState({
                 email:email
             })
    }
    onSubmitforgetPassword = async()=>{
        let {email} = this.state
        forgetPassword(email).then(response=>{
            if(response.data.error==true){
                alert(response.data.errorText)
            }else{
                Alert.alert(
                    'Success',
                    `${response.data.message}`,
                    [
                      { text: 'GO BACK TO LOGIN', onPress: () => this.props.navigation.navigate("LoginScreen")},
                    ],
                    { cancelable: false }
                  ); 
                  email="";
                  this.setState({
                      email:email
                  })                 
            }
        }).catch(error=>{console.log(error,"throwing error")})
    }
    render() {
        let { email } = this.state
        return (
            <View style={{ height: "100%", width: "100%", padding: 25 }}>
                <View style={{ height: "10%", width: "100%" }}>
                    <TextInputField
                        keyboardType={'default'}
                        onChangeText={this.onChangeText}
                        value={email}
                        labelName={"Email"}
                        name={"email"}
                        labelStyle={{ fontSize: 20 }}
                        parentStyle={{ height: "80%", width: "100%" }}
                        childStyle={{ height: "50%", width: "100%", borderBottomWidth: 1 }}
                        key={"email"}
                    />
                </View>
                <View style={{ height: "10%", width: "100%", flexDirection: "row", justifyContent: "flex-end" }}>
                    <SubmitButton
                        style={{
                            height: "80%", width: "30%", alignItems: "center",
                            borderRadius: 15, justifyContent: "center", backgroundColor: "#AEEEB1"
                        }}
                        onPress={this.onSubmitforgetPassword}
                        title="Submit" />
                </View>
            </View>
        );
    }
}
